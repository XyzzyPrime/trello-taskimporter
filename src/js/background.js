chrome.runtime.onInstalled.addListener(function(details) {
    if (details.reason === 'install') {
        chrome.tabs.create({'url': chrome.extension.getURL('settings.html')});
    }
});

function setPopupForTab(tabId) {
    if (getToken()) {
        chrome.pageAction.setPopup({'tabId': tabId, 'popup': 'popup.html'});
    }
}

chrome.tabs.onActivated.addListener(function(activeInfo) {
    setPopupForTab(activeInfo.tabId);
});

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab) {
    setPopupForTab(tabId);
});

chrome.pageAction.onClicked.addListener(function(tab) {
    chrome.tabs.create({'url': chrome.extension.getURL('settings.html')});
});
